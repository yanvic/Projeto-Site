const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

  //SENIORIDADE 
  async function cadastrarSenioridade(descricao, cargo, cidadeId, graduacao, unidadeId, modalidadeId, categoriaId) {
    try {
      // Criação da senioridade
      const senioridade = await prisma.senioridade.create({
        data: {
          descricao: descricao,
          cargo: cargo,
          cidade_id: cidadeId,
          graduacao: graduacao,
          unidadeId: unidadeId,
          Modalidade: modalidade,
        }
      });
  
      console.log("Senioridade cadastrada com sucesso:", senioridade);
    } catch (error) {
      console.error("Erro ao cadastrar senioridade:", error);
    }
  }
  
  // Exemplo de uso da função
  const descricao = "Desenvolvedor Junior";
  const cargo = "Desenvolvedor";
  const cidadeId = 1;
  const graduacao = "Ads";
  const unidadeId = 1;
  const modalidade = "NOTURNO";
  
  cadastrarSenioridade(descricao, cargo, cidadeId, graduacao, unidadeId, modalidade,);
  
  