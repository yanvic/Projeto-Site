/*
  Warnings:

  - You are about to drop the `Categorias` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Linguagem` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Unidade` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "senioridade" DROP CONSTRAINT "senioridade_categoriaId_fkey";

-- DropForeignKey
ALTER TABLE "senioridade" DROP CONSTRAINT "senioridade_unidadeId_fkey";

-- DropForeignKey
ALTER TABLE "usuarios" DROP CONSTRAINT "usuarios_linguagem_id_fkey";

-- DropTable
DROP TABLE "Categorias";

-- DropTable
DROP TABLE "Linguagem";

-- DropTable
DROP TABLE "Unidade";

-- CreateTable
CREATE TABLE "categorias" (
    "id" SERIAL NOT NULL,
    "descricao" TEXT NOT NULL,

    CONSTRAINT "categorias_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "linguagem" (
    "id" SERIAL NOT NULL,
    "nome" TEXT NOT NULL,

    CONSTRAINT "linguagem_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "unidade" (
    "Id" SERIAL NOT NULL,
    "nome" TEXT NOT NULL,
    "rua" TEXT NOT NULL,
    "numero" TEXT NOT NULL,
    "bairro" TEXT NOT NULL,
    "cep" DOUBLE PRECISION NOT NULL,
    "complemento" TEXT NOT NULL,
    "estado" TEXT NOT NULL,
    "cidade" TEXT NOT NULL,

    CONSTRAINT "unidade_pkey" PRIMARY KEY ("Id")
);

-- AddForeignKey
ALTER TABLE "usuarios" ADD CONSTRAINT "usuarios_linguagem_id_fkey" FOREIGN KEY ("linguagem_id") REFERENCES "linguagem"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "senioridade" ADD CONSTRAINT "senioridade_categoriaId_fkey" FOREIGN KEY ("categoriaId") REFERENCES "categorias"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "senioridade" ADD CONSTRAINT "senioridade_unidadeId_fkey" FOREIGN KEY ("unidadeId") REFERENCES "unidade"("Id") ON DELETE RESTRICT ON UPDATE CASCADE;
