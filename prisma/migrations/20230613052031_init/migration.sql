/*
  Warnings:

  - You are about to drop the column `categoriaId` on the `senioridade` table. All the data in the column will be lost.
  - You are about to drop the `categorias` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "senioridade" DROP CONSTRAINT "senioridade_categoriaId_fkey";

-- AlterTable
ALTER TABLE "senioridade" DROP COLUMN "categoriaId";

-- DropTable
DROP TABLE "categorias";
