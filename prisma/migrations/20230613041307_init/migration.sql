/*
  Warnings:

  - You are about to drop the column `descriao` on the `Categorias` table. All the data in the column will be lost.
  - You are about to drop the `Financeiro` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `unidade` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `descricao` to the `Categorias` table without a default value. This is not possible if the table is not empty.
  - Added the required column `categoriaId` to the `senioridade` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Categorias" DROP COLUMN "descriao",
ADD COLUMN     "descricao" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "senioridade" ADD COLUMN     "categoriaId" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "usuarios" ALTER COLUMN "deleted_at" DROP NOT NULL;

-- DropTable
DROP TABLE "Financeiro";

-- DropTable
DROP TABLE "unidade";

-- CreateTable
CREATE TABLE "financeiro" (
    "id" SERIAL NOT NULL,
    "userID" INTEGER NOT NULL,
    "mensalidade" DOUBLE PRECISION NOT NULL,
    "bolsista" BOOLEAN NOT NULL,
    "desconto" DOUBLE PRECISION NOT NULL,
    "status" TEXT NOT NULL,

    CONSTRAINT "financeiro_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Linguagem" (
    "id" SERIAL NOT NULL,
    "nome" TEXT NOT NULL,

    CONSTRAINT "Linguagem_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Unidade" (
    "Id" SERIAL NOT NULL,
    "nome" TEXT NOT NULL,
    "rua" TEXT NOT NULL,
    "numero" TEXT NOT NULL,
    "bairro" TEXT NOT NULL,
    "cep" DOUBLE PRECISION NOT NULL,
    "complemento" TEXT NOT NULL,
    "estado" TEXT NOT NULL,
    "cidade" TEXT NOT NULL,

    CONSTRAINT "Unidade_pkey" PRIMARY KEY ("Id")
);

-- AddForeignKey
ALTER TABLE "usuarios" ADD CONSTRAINT "usuarios_linguagem_id_fkey" FOREIGN KEY ("linguagem_id") REFERENCES "Linguagem"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "usuarios" ADD CONSTRAINT "usuarios_senioridade_id_fkey" FOREIGN KEY ("senioridade_id") REFERENCES "senioridade"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "senioridade" ADD CONSTRAINT "senioridade_categoriaId_fkey" FOREIGN KEY ("categoriaId") REFERENCES "Categorias"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "senioridade" ADD CONSTRAINT "senioridade_unidadeId_fkey" FOREIGN KEY ("unidadeId") REFERENCES "Unidade"("Id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "financeiro" ADD CONSTRAINT "financeiro_userID_fkey" FOREIGN KEY ("userID") REFERENCES "usuarios"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
