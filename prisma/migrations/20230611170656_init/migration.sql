-- CreateTable
CREATE TABLE "usuarios" (
    "id" SERIAL NOT NULL,
    "nome" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "senha" TEXT NOT NULL,
    "linguagem_id" INTEGER NOT NULL,
    "senioridade_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "deleted_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "usuarios_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "visitante" (
    "id" SERIAL NOT NULL,
    "acess_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "visitante_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "senioridade" (
    "id" SERIAL NOT NULL,
    "descricao" TEXT NOT NULL,
    "cargo" TEXT NOT NULL,
    "cidade_id" INTEGER NOT NULL,
    "graduacao" TEXT NOT NULL,
    "unidadeId" INTEGER NOT NULL,
    "ModalidadeID" INTEGER NOT NULL,

    CONSTRAINT "senioridade_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Categorias" (
    "id" SERIAL NOT NULL,
    "descriao" TEXT NOT NULL,

    CONSTRAINT "Categorias_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Financeiro" (
    "id" SERIAL NOT NULL,
    "userID" INTEGER NOT NULL,
    "mensalidade" DOUBLE PRECISION NOT NULL,
    "bolsista" BOOLEAN NOT NULL,
    "desconto" DOUBLE PRECISION NOT NULL,
    "status" TEXT NOT NULL,

    CONSTRAINT "Financeiro_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "unidade" (
    "Id" SERIAL NOT NULL,
    "nome" TEXT NOT NULL,
    "rua" TEXT NOT NULL,
    "numero" TEXT NOT NULL,
    "bairro" TEXT NOT NULL,
    "cep" DOUBLE PRECISION NOT NULL,
    "complemento" TEXT NOT NULL,
    "estado" TEXT NOT NULL,
    "cidade" TEXT NOT NULL,

    CONSTRAINT "unidade_pkey" PRIMARY KEY ("Id")
);

-- CreateIndex
CREATE UNIQUE INDEX "usuarios_email_key" ON "usuarios"("email");
