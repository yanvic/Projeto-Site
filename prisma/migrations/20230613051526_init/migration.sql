/*
  Warnings:

  - You are about to drop the column `ModalidadeID` on the `senioridade` table. All the data in the column will be lost.
  - Added the required column `Modalidade` to the `senioridade` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "senioridade" DROP COLUMN "ModalidadeID",
ADD COLUMN     "Modalidade" TEXT NOT NULL;
