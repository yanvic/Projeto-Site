const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

  //fINANCEIRO
  async function cadastrarRegistroFinanceiro(userID, mensalidade, bolsista, desconto, status) {
    try {
      // Criação do registro financeiro
      const registroFinanceiro = await prisma.financeiro.create({
        data: {
          userID: userID,
          mensalidade: mensalidade,
          bolsista: bolsista,
          desconto: desconto,
          status: status
        }
      });
  
      console.log("Registro financeiro cadastrado com sucesso:", registroFinanceiro);
    } catch (error) {
      console.error("Erro ao cadastrar registro financeiro:", error);
    }
  }
  
  // Exemplo de uso da função
  const userID = 2;
  const mensalidade = 1500.0;
  const bolsista = false;
  const desconto = 300.0;
  const status = "Pago";
  
  cadastrarRegistroFinanceiro(userID, mensalidade, bolsista, desconto, status);

