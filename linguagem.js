const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

  async function cadastrarLinguagem(nome) {
    try {
      // Criação do registro de linguagem
      const linguagem = await prisma.linguagem.create({
        data: {
          nome: nome
        }
      });
  
      console.log("Linguagem cadastrada com sucesso:", linguagem);
    } catch (error) {
      console.error("Erro ao cadastrar linguagem:", error);
    }
  }
  
  // Exemplo de uso da função
  const nomeLinguagem = "python";
  
  cadastrarLinguagem(nomeLinguagem);

 