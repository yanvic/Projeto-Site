
const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();
async function cadastrarUsuario(nome, email, senha, linguagemId, senioridadeId) {
    try {
      // Criação do usuário
      const usuario = await prisma.usuarios.create({
        data: {
          nome,
          email,
          senha,
          linguagem_id: linguagemId,
          senioridade_id: senioridadeId
        }
      });
  
      console.log("Usuário cadastrado com sucesso:", usuario);
    } catch (error) {
      console.error("Erro ao cadastrar usuário:", error);
    }
  }
  
  // Exemplo de uso da função
  cadastrarUsuario("John Doe", "johndoe@gmail.com", "senha123", 1, 1);


