const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

  //
  async function cadastrarUnidade(nome, rua, numero, bairro, cep, complemento, estado, cidade) {
    try {
      // Criação do registro de unidade
      const unidade = await prisma.unidade.create({
        data: {
          nome: nome,
          rua: rua,
          numero: numero,
          bairro: bairro,
          cep: cep,
          complemento: complemento,
          estado: estado,
          cidade: cidade
        }
      });
  
      console.log("Unidade cadastrada com sucesso:", unidade);
    } catch (error) {
      console.error("Erro ao cadastrar unidade:", error);
    }
  }
  
  // Exemplo de uso da função
  const nomeUnidade = "Unidade A";
  const ruaUnidade = "Rua Principal";
  const numeroUnidade = "123";
  const bairroUnidade = "Centro";
  const cepUnidade = 12345678;
  const complementoUnidade = "Sala 1";
  const estadoUnidade = "SP";
  const cidadeUnidade = "São Paulo";
  
  cadastrarUnidade(
    nomeUnidade,
    ruaUnidade,
    numeroUnidade,
    bairroUnidade,
    cepUnidade,
    complementoUnidade,
    estadoUnidade,
    cidadeUnidade
  );
  
  
  
  